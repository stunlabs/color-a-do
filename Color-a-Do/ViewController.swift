//
//  ViewController.swift
//  Color-a-Do
//
//  Created by Jörg Klausewitz on 05.08.15.
//  Copyright (c) 2015 Stunlabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Color views
    
    // Shade colors
    @IBOutlet weak var colorShade1: UIView!
    @IBOutlet weak var colorShade2: UIView!
    @IBOutlet weak var colorShade3: UIView!
    
    // Main color
    @IBOutlet weak var colorMain: UIView!

    // Tint colors
    @IBOutlet weak var colorTint1: UIView!
    @IBOutlet weak var colorTint2: UIView!
    @IBOutlet weak var colorTint3: UIView!
    
    // Shade labels
    @IBOutlet weak var labelShade1: UILabel!
    @IBOutlet weak var labelShade2: UILabel!
    @IBOutlet weak var labelShade3: UILabel!
    
    // Main label
    @IBOutlet weak var labelMain: UILabel!
    
    // Tint labels
    @IBOutlet weak var labelTint1: UILabel!
    @IBOutlet weak var labelTint2: UILabel!
    @IBOutlet weak var labelTint3: UILabel!
    
    // Sliders
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    // RGB labels
    @IBOutlet weak var labelRed: UILabel!
    @IBOutlet weak var labelGreen: UILabel!
    @IBOutlet weak var labelBlue: UILabel!
    
    
    // Buttons
    @IBOutlet weak var bluePlusButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorMain.backgroundColor = UIColor.blackColor()
        
        labelMain.textColor = UIColor.blackColor()
        
        // Appearance of sliders
        redSlider.minimumTrackTintColor = UIColor.redColor()
        redSlider.thumbTintColor = UIColor.redColor()
        
        // Custom green
        let darkGreenColor = UIColor(red: 27/255, green: 143/255, blue: 76/255, alpha: 1)
    
        greenSlider.minimumTrackTintColor = darkGreenColor
        greenSlider.thumbTintColor = darkGreenColor
        
        labelGreen.textColor = darkGreenColor

        blueSlider.thumbTintColor = UIColor.blueColor()
        
        
        
        
    }
    
    @IBAction func changeColorComponent(sender: AnyObject) {
        
        let redCGF: CGFloat = CGFloat(self.redSlider.value)
        let greenCGF: CGFloat = CGFloat(self.greenSlider.value)
        let blueCGF: CGFloat = CGFloat(self.blueSlider.value)
        
        updateValues(redCGF, greenCGF: greenCGF, blueCGF: blueCGF)
        
        
    }
    

    
    //Gives a string of a hex value back. Accept rgb values.
    func RGBToHEX( re: CGFloat, gr: CGFloat, bl: CGFloat) -> String {
        let hexValue:String
        var redHex = String(NSString(format:"%2X", Int( re * 255 ) )).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if count(redHex) == 1 {redHex = "0"+redHex}
        
        var greenHex = String(NSString(format:"%2X", Int( gr * 255 ) )).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if count(greenHex) == 1 {greenHex = "0"+greenHex}
        
        var blueHex = String(NSString(format:"%2X", Int( bl * 255 ) )).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if count(blueHex) == 1 {blueHex = "0"+blueHex}
        

        hexValue = "#" + redHex + greenHex + blueHex
        
        return hexValue
    }
    
    
    // Returns a tupel of a UIColor and a hexadecimal value string
    func shadesNTints(red: CGFloat, green: CGFloat, blue: CGFloat, steps: CGFloat, stage: CGFloat) -> (color: UIColor, hex: String) {
        
        let color:UIColor
        let hex:String
        
        let st = stage+1
        
        var newRed:CGFloat = 0
        var newGreen:CGFloat = 0
        var newBlue:CGFloat = 0
        
        let max:CGFloat = 1
        
        let redStep = (max - red) / steps
        let greenStep = (max - green) / steps
        let blueStep = (max - blue) / steps
        
        let factor = (1/steps) * abs(stage)
        
        
        if stage < 0 {
            newRed = red + (1 - red) * factor
            newGreen = green + (1 - green) * factor
            newBlue = blue + (1 - blue) * factor
        } else {
            newRed = red * (1 - factor)
            newGreen = green * (1 - factor)
            newBlue = blue * (1 - factor)
        }

        
        hex = RGBToHEX(newRed, gr: newGreen, bl: newBlue)
        
        color = UIColor(red: newRed, green: newGreen, blue: newBlue, alpha: 1)
        
        let colorArr = [newRed, newGreen, newBlue, 1]

        return (color, hex)
    
    }
    
    
    func updateValues(redCGF: CGFloat, greenCGF: CGFloat, blueCGF:CGFloat) {
    
    
        // Slider CGF values as RGB value 0-255
        let redRGB = Int(redCGF*255)
        let greenRGB = Int(greenCGF*255)
        let blueRGB = Int(blueCGF*255)
        labelRed.text = "\(redRGB)"
        labelGreen.text = "\(greenRGB)"
        labelBlue.text = "\(blueRGB)"
        
        
        // Color created by sliders
        let mainColor = UIColor(red: redCGF, green: greenCGF, blue: blueCGF, alpha: 1)
        
        let steps:CGFloat = 10
        
        colorShade1.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: -3).color
        labelShade1.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: 6, stage: -3).hex
        
        colorShade2.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: -2).color
        labelShade2.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: -2).hex
        
        colorShade3.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: -1).color
        labelShade3.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: -1).hex
        
        
        colorMain.backgroundColor = mainColor
        
        
        colorTint1.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 1).color
        labelTint1.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 1).hex
        
        colorTint2.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 2).color
        labelTint2.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 2).hex
        
        colorTint3.backgroundColor = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 3).color
        labelTint3.text = shadesNTints(redCGF, green: greenCGF, blue: blueCGF, steps: steps, stage: 3).hex
        
        
        
        let hexD = RGBToHEX( redCGF, gr: greenCGF, bl: blueCGF )
        
        labelMain.text = hexD
    
    
    
    
    }



}

